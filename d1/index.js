/* 
placeholder API: https://jsonplaceholder.typicode.com/posts
*/


fetch('https://jsonplaceholder.typicode.com/posts').then((response)=> response.json()).then((data) => showPosts(data));



const showPosts = (posts) => {
	let postEntries = '';

	posts.forEach((post)=>{
		// in javascript it is also possible to render html elements with the use of functions/methods plus appropriate syntax such as template literals (backticks plus ${});
        postEntries += `
			<div id = "post-${post.id}">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button onclick="editPost(${post.id})">Edit</button>
				<button onclick="deletePost(${post.id})">Delete</button>
			</div>
		`
	});
	// the div duplicates the objects inside the postEntries variable
	document.querySelector('#div-post-entries').innerHTML = postEntries;
};

// ADD POST DATA

document.querySelector('#form-add-post').addEventListener('submit',(e)=>{
    e.preventDefault();

    fetch("https://jsonplaceholder.typicode.com/posts", {
        method: "POST",
        body: JSON.stringify({
            title: document.querySelector('#txt-title').ariaValueMax,
            body: document.querySelector('#txt-body').value,
            userId: 1
        }),
        headers: {'Content-type': 'application/json; charset=UTF-8'}
    })
    .then((response) => response.json())
    .then((data) => {
        console.log(data);
        alert('Successfully added');


        document.querySelector('#txt-title').value = null;
        document.querySelector('#txt-body').value = null;
})

});

// Edit post

const editPost = (id) =>{
	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	document.querySelector('#txt-edit-id').value = id;
	document.querySelector('#txt-edit-title').value = title;
	document.querySelector('#txt-edit-body').value = body;
    // removeAttribute removes the disabled attribute of the update button
	document.querySelector('#btn-submit-update').removeAttribute('disabled');
};


document.querySelector('#form-edit-post').addEventListener('submit', (e)=>{
	e.preventDefault();


   
    fetch("https://jsonplaceholder.typicode.com/posts/1", {
        method: "PUT",
        body: JSON.stringify({
            id: document.querySelector('#txt-edit-id'),
            title: document.querySelector('#txt-edit-title').ariaValueMax,
            body: document.querySelector('#txt-edit-body').value,
            userId: 1
        }),
        headers: {'Content-type': 'application/json; charset=UTF-8'}
    })
	.then((response) => response.json())
    .then((data) => {
        console.log(data);
        alert('Successfully updated.');


        document.querySelector(`#post-title-${data.id}`).innerHTML = data.title;
        document.querySelector(`#post-body-${data.id}`).innerHTML = data.body;


        document.querySelector('#txt-edit-id').value = null;
        document.querySelector('#txt-edit-title').value = null;
        document.querySelector('#txt-edit-body').value = null;
        // setAttrobute allows us to set up attribute or bring back the 'disabled' att we removed earlier
        // it is accepting 2 arguments 1st - the string idnetifies the attribute to be set and 
        document.querySelector('#btn-submit-update').setAttribute('disabled',true);
})
});


// Activity
// delete post


const deletePost = (id) => {

    let posts = fetch("https://jsonplaceholder.typicode.com/posts", {
        method: "GET",
        headers: {'Content-type': 'application/json; charset=UTF-8'}
    })
    .then((response) => response.json())
    .then((data) => {
        console.log(data);
        document.querySelector(`#post-${id}`).remove();
});
}
